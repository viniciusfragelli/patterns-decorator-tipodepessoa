package Pessoas;

import java.util.ArrayList;

public class Pessoa {

	protected String nome;
	protected String matricula;
	protected ArrayList<String> tipo = new ArrayList<String>();
	protected boolean bolsista;
	
	public Pessoa(String nome, String matricula, boolean bol) {
		this.nome = nome;
		this.matricula = matricula;
		bolsista = bol;
	}
	
	public Pessoa() {
		
	}
	
	public String getNome(){
		return nome;
	}
	
	public String getMatricula(){
		return matricula;
	}

	public ArrayList<String> getTipoDecorator(){
		return new ArrayList<String>();
	}
	
	public Pessoa getDecorator(){
		return null;
	}
	
	public boolean isBolsista(){
		return bolsista;
	}
	
	public void setPessoa(Pessoa p){
		
	}
}
