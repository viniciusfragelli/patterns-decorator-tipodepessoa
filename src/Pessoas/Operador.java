package Pessoas;

import java.util.ArrayList;

public class Operador extends Pessoa{

	private Pessoa p;
	
	public Operador(Pessoa p) {
		super();
		this.p = p;
	}
	
	@Override
	public String getNome() {
		return p.getNome();
	}

	@Override
	public String getMatricula() {
		return p.getMatricula();
	}

	@Override
	public ArrayList<String> getTipoDecorator() {
		tipo = p.getTipoDecorator();
		tipo.add("Operador");
		return tipo;
	}

	public Pessoa getDecorator(){
		return p;
	}
	
	public boolean isBolsista(){
		return p.isBolsista();
	}
	
	public void setPessoa(Pessoa p){
		this.p = p;
	}
}
