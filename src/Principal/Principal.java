package Principal;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Pessoas.Aluno;
import Pessoas.Gerente;
import Pessoas.Operador;
import Pessoas.Pessoa;

public class Principal {
	
	private List<Pessoa> pessoa = new ArrayList<>();
	private Scanner s = new Scanner(System.in);
	
	public Principal() {
		boolean controle = true;
		
		while(controle){
			System.out.println("Entre com a op��o:");
			System.out.println("1 - Cadastro\n"
					+ "2 - Matricula\n"
					+ "3 - Contratacao\n"
					+ "4 - Trancamento\n"
					+ "5 - Demissao\n"
					+ "6 - Calcular Mensalidade\n"
					+ "7 - Listar alunos (Nome e matricula)\n"
					+ "8 - Listar funcion�rios\n"
					+ "9 - Listar somente gerentes\n"
					+ "10 - Sair");
			
			int k = s.nextInt();
			if(k > 0 && k < 11){
				switch(k){
					case 1: cadastrar(); break;
					case 2: matricular(); break;
					case 3: contratacao(); break;
					case 4: trancamento(); break;
					case 5: demissao(); break;
					case 6: calcularMensalidade(); break;
					case 7: listarAlunos(); break;
					case 8: listarFuncionarios(); break;
					case 9: listarGerentes(); break;
					case 10: System.exit(1); break;
				}
			}else{
				System.out.println("Valor invalido!");
			}
		}
	}
	
	private int tipoDeFuncionario(){
		while(true){
			System.out.println("Tipo de funcionario:\n1-Operador\n2-Gerente");
			int k = s.nextInt();
			if(k == 1 || k == 2){
				return k;
			}
		}
	}
	
	public void demissao(){
		if(listarFuncionarios()){
			System.out.println("Seleciona um aluno para demitir: ");
			int k = s.nextInt()-1;
			ArrayList<String> aux = pessoa.get(k).getTipoDecorator();
			if(aux.size() == 1){
				pessoa.set(k, pessoa.get(k).getDecorator());
			}else{
				if(aux.get(1).equalsIgnoreCase("operador") || aux.get(1).equalsIgnoreCase("gerente")){
					pessoa.set(k, pessoa.get(k).getDecorator());
				}else{
					pessoa.get(k).setPessoa(pessoa.get(k).getDecorator().getDecorator());
				}
			}
		}
	}
	
	public boolean getBolsista(){
		while(true){
			System.out.println("� bolsista?\n1- sim\n2- n�o");
			int k = s.nextInt();
			if(k == 1 ){
				return true;
			}else{
				return false;
			}
		}
	}
	
	public void cadastrar(){
		System.out.println("Entre com o nome:");
		String nome = s.next();
		System.out.println("Entre com a matricula: ");
		String mat = s.next();
		boolean bolsa = getBolsista();
		Pessoa p = new Pessoa(nome, mat, bolsa);
		pessoa.add(p);
	}
	
	public void matricular(){
		if(listarFuncionarioEPessoas()){
			System.out.println("Selecione uma pessoa: ");
			int k = s.nextInt()-1;
			pessoa.set(k, new Aluno(pessoa.get(k)));
		}
	}
	
	public void contratacao(){
		if(listarAlunoEPessoa()){
			System.out.println("Selecione um aluno: ");
			int k = s.nextInt()-1;
			int tipo = tipoDeFuncionario();
			if(tipo == 1){
				pessoa.set(k, new Operador(pessoa.get(k)));
			}else{
				pessoa.set(k, new Gerente(pessoa.get(k)));
			}
		}
	}
	
	public void calcularMensalidade(){
		if(listarTudo()){
			System.out.println("Selecione uma pessoa: ");
			int k = s.nextInt()-1;
			ArrayList<String> tipo = pessoa.get(k).getTipoDecorator();
			float men = 1000;
			System.out.println("Valor pago: ");
			if(tipo.get(tipo.size()-1).equalsIgnoreCase("operador")){
				if(pessoa.get(k).isBolsista()){
					men = (float) (men*0.7);
				}else{
					men = (float) (men*0.5);
				}
			}else{
				if(tipo.get(tipo.size()-1).equalsIgnoreCase("gerente")){
					if(pessoa.get(k).isBolsista()){
						men = (float) (men*0.4);
					}else{
						men = (float) (men*0.6);
					}
				}else{
					if(tipo.get(tipo.size()-1).equalsIgnoreCase("aluno")){
						if(pessoa.get(k).isBolsista()){
							men = (float) (men*0.7);
						}
					}
				}
			}
			System.out.println(men+"");
		}
	}
	
	public boolean listarAlunoEPessoa(){
		if(!pessoa.isEmpty()){
			ArrayList <String> aux;
			boolean teste = false;
			boolean imp = false;
			for (int i = 0; i < pessoa.size(); i++) {
				aux = pessoa.get(i).getTipoDecorator();
				if(aux.isEmpty()){
					teste = true;
					System.out.println((i+1)+" - "+pessoa.get(i).getNome()+" "+pessoa.get(i).getMatricula());
				}else{
					imp = true;
					for (int j = 0; j < aux.size(); j++) {
						if(aux.get(j).equalsIgnoreCase("operador") || aux.get(j).equalsIgnoreCase("gerente")){
							imp = false;
						}
					}
					if(imp){
						System.out.println((i+1)+" - "+pessoa.get(i).getNome()+" "+pessoa.get(i).getMatricula());
						teste = true;
					}
				}
			}
			return teste;
		}else{
			return false;
		}
	}

	public boolean listarAlunos(){
		if(!pessoa.isEmpty()){
			ArrayList <String> aux;
			boolean teste = false;
			for (int i = 0; i < pessoa.size(); i++) {
				aux = pessoa.get(i).getTipoDecorator();
				for (int j = 0; j < aux.size(); j++) {
					if(aux.get(j).equalsIgnoreCase("aluno")){
						teste = true;
						System.out.println((i+1)+" - "+pessoa.get(i).getNome()+" "+pessoa.get(i).getMatricula());
					}
				}
			}
			return teste;
		}else{
			return false;
		}
	}
	
	public boolean listarFuncionarios(){
		if(!pessoa.isEmpty()){
			ArrayList <String> aux;
			boolean teste = false;
			for (int i = 0; i < pessoa.size(); i++) {
				aux = pessoa.get(i).getTipoDecorator();
				for (int j = 0; j < aux.size(); j++) {
					if(aux.get(j).equalsIgnoreCase("operador") || aux.get(j).equalsIgnoreCase("gerente")){
						teste = true;
						System.out.println((i+1)+" - "+pessoa.get(i).getNome()+" "+pessoa.get(i).getMatricula());
					}
				}
			}
			return teste;
		}else{
			return false;
		}
	}
	
	public boolean listarFuncionarioEPessoas(){
		if(!pessoa.isEmpty()){
			ArrayList <String> aux;
			boolean teste = false;
			boolean imp = false;
			for (int i = 0; i < pessoa.size(); i++) {
				aux = pessoa.get(i).getTipoDecorator();
				if(aux.isEmpty()){
					System.out.println((i+1)+" - "+pessoa.get(i).getNome()+" "+pessoa.get(i).getMatricula());
					teste = true;
				}else{
					imp = true;
					for (int j = 0; j < aux.size(); j++) {
						if(aux.get(j).equalsIgnoreCase("aluno")){
							imp = false;
						}
					}
					if(imp){
						teste = true;
						System.out.println((i+1)+" - "+pessoa.get(i).getNome()+" "+pessoa.get(i).getMatricula());
					}
				}
			}
			return teste;
		}else{
			return false;
		}
	}
	
	public boolean listarTudo(){
		if(!pessoa.isEmpty()){
			for (int i = 0; i < pessoa.size(); i++) {
				System.out.println((i+1)+" - "+pessoa.get(i).getNome()+" "+pessoa.get(i).getMatricula());
			}
			return true;
		}else{
			return false;
		}
	}
	
	public void listarGerentes(){
		ArrayList <String> aux;
		boolean teste = false;
		for (int i = 0; i < pessoa.size(); i++) {
			aux = pessoa.get(i).getTipoDecorator();
			if(aux.get(aux.size()-1).equalsIgnoreCase("gerente")){
				teste = true;
				System.out.println((i+1)+" - "+pessoa.get(i).getNome()+" "+pessoa.get(i).getMatricula());
			}
		}
	}
	
	public void trancamento(){
		if(listarAlunos()){
			System.out.println("Seleciona um aluno para trancar: ");
			int k = s.nextInt()-1;
			ArrayList<String> aux = pessoa.get(k).getTipoDecorator();
			if(aux.size() == 1){
				pessoa.set(k, pessoa.get(k).getDecorator());
			}else{
				if(aux.get(1).equalsIgnoreCase("aluno")){
					pessoa.set(k, pessoa.get(k).getDecorator());
				}else{
					pessoa.get(k).setPessoa(pessoa.get(k).getDecorator().getDecorator());
				}
			}
		}
	}
}
